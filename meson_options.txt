option(
  'installed_tests',
  type: 'boolean',
  value: false,
  description: 'enable installed tests'
)
option(
  'pamlibdir',
  type: 'string',
  description: 'directory for PAM modules'
)
